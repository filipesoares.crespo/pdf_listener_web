import { MLanguage } from 'vue-multilanguage'

export default new MLanguage('portuguese').create({
  title: 'Oi {0}!',
  msg: 'Você tem {f} amigos e {l} curtidas'
})
