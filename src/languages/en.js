import { MLanguage } from 'vue-multilanguage'

export default new MLanguage('english').create({
  forgotPassword: 'Forgot your Password?',
  createUser: 'Create user? ',
  RememberMe: 'Remember-messs ',
  haveAccount: 'I have account! ',
})
