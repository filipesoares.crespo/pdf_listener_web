import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    usuario:[],
  },
  mutations: {
    setUsuario(state,n){
      state.usuario = n;
    },
  },
  actions: {
    getUsuario: state =>{
      return state.usuario;
    }
  },
  modules: {

  }
})
