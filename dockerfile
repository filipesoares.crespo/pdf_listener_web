FROM node

WORKDIR /app

RUN npm install -g jshint \
    npm install -g jest

COPY . .

RUN yarn install

CMD ["yarn", "serve"]