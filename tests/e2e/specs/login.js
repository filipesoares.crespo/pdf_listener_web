// For authoring Nightwatch tests, see
// http://nightwatchjs.org/guide#usage

module.exports = {
  // 'default e2e tests': browser => {
  //   browser
  //     .url(browser.launchUrl.'/login')
  //     .waitForElementVisible('#app', 5000)
  //     .assert.elementPresent('.hello')
  //     .assert.containsText('h1', 'Welcome to Your Vue.js App')
  //     .assert.elementCount('img', 1)
  //     .end()
  // }
  'successful login': browser => {
    browser
      .url(browser.launchUrl+'/login')
      .waitForElementVisible('#app', 5000)
      .assert.visible('input[type=email]')
      .setValue('input[type=email]', 'f@f.com')
      .assert.visible('input[type=password]')
      .setValue('input[type=password]', '123456')
      .click('button[id=signin]')
      .waitForElementVisible('#template-principal', 5000)
      .end()
  },
  'falid login': browser => {
    browser
      .url(browser.launchUrl+'/login')
      .waitForElementVisible('#app', 5000)
      .assert.visible('input[type=email]')
      .setValue('input[type=email]', 'f@f.com')
      .assert.visible('input[type=password]')
      .setValue('input[type=password]', '123')
      .click('button[id=signin]')
      .waitForElementVisible('#messagee', 5000)
      .end()
  },
}
